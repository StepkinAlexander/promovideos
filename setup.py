#!/usr/bin/env python
from distutils.core import setup
dist = setup(
    name='promovideos',
    version='1.0',
    description = 'getting promo films',
    long_description = 'getting promo films from known video-services',
    author='Aleksandr Stepkin',
    author_email='stepkin@jetstyle.ru',
    url='..',
    license='gpl',

    package_dir={'':'src'},
    packages=['promovideos'],

    data_files=[('/etc/promovideos', ['install/config.json-default']),
                ('/usr/bin',['install/promovideosd'])]
)
