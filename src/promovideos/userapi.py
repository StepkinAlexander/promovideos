#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
from datetime import datetime

from twisted.web import resource
from urllib import unquote

from promovideos import saveStatus, dateFormat

class UserApi(resource.Resource):
    isLeaf = True
    __logger = None
    __collector = None
    def __init__(self, logger, collector):
        resource.Resource.__init__(self)
        self.__collector = collector
        self.__logger = logger

    def render_GET(self, request):
        def incorrectCommand(params=None):
            return 'Incorrect request'
        uri = unquote(request.uri[1:]) if request.uri[0:1]=='/' else unquote(request.uri)
        if not uri[-1:]=='/': uri = '%s/' % uri

        if uri != 'favicon.ico/':
            self.__logger.info('GET request: %s (from %s)', uri, request.getClientIP())
            saveStatus(request=uri, requesttime=datetime.now().strftime(dateFormat))

        result = self.__collector.getInfo()
        if uri != 'favicon.ico/':
            saveStatus(answer=result, answertime=datetime.now().strftime(dateFormat))
        return result
