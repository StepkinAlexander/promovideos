#!/usr/bin/python
# -*- coding: utf-8 -*-
import json

#config
try:
    config = json.load(open('/etc/promovideos/config.json', 'r'))
except:
    config = dict()

dateFormat = '%Y-%m-%d %H:%M:%S'

#SERVICE STATUS
__status = dict(
    updatestatus = '',
    updatetime = '',
    request = '',
    requesttime = '',
    answer = '',
    answertime = '',
    service = '',
    config = config,
)
def saveStatus(updatestatus = None, updatetime = None,
               request = None, requesttime = None,
               answer = None, answertime = None,
               service = None):
    if not config.get('statusdest', ''):
        #if not needance
        return
    if updatestatus:
        __status['updatestatus'] = updatestatus
    if updatetime:
        __status['updatetime'] = updatetime
    if request:
        __status['request'] = request
    if requesttime:
        __status['requesttime'] = requesttime
    if answer:
        __status['answer'] = answer
    if answertime:
        __status['answertime'] = answertime
    if service:
        __status['service'] = service
    w = open(config.get('statusdest'), 'w')
    w.write(json.dumps(__status))
    w.close()
