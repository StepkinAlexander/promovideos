#!/usr/bin/python
# -*- coding: utf-8 -*-
import signal

#logging
import logging
import logging.handlers
log = logging.getLogger('promovideos')

from twisted.web import server
from twisted.web.client import getPage
from twisted.internet import reactor, threads

import json
import re
from datetime import datetime

from promovideos import dateFormat, saveStatus, config as _config_
from promovideos.userapi import UserApi

import random


class Collector(object):
    __running = None
    __films = dict()
    __refreshtime = _config_.get('parse', dict()).get('refreshtime', 1)*60  # at minutes
    __urls = _config_.get('parse', dict()).get('url', list())

    def __init__(self, logger):
        self.__logger = logger
        self.__logger.info('Urls received from config: %s ' % self.__urls)

    def getInfo(self):
        #get info to user
        return json.dumps(self.__films)

    def collect(self):
        #collecting promo films info
        def collectUrlDone(r, service):
            if service == 'rutube':
                res = json.loads(r).get('result')[0]
                film = dict(
                    id=res.get('id'),
                    poster=res.get('imageWideBigUrl'),
                    name=res.get('title'),
                )
            elif service == 'zoomby':
                res = random.choice(json.loads(r).get('result'))
                film = dict(
                    id=res.get('id'),
                    poster=res.get('poster') if res.get('poster') else res.get('preview'),
                    name=res.get('name'),
                    smil=res.get('url'),
                )
            elif service == 'ivi':
                res = json.loads(r)[0]
                film = dict(
                    id=res.get('id'),
                    poster=res.get('img_wp7').get('path'),
                    name=res.get('title'),
                )
            elif service == 'tvigle':
                res = json.loads(r)[0]
                film = dict(
                    id=res.get('id'),
                    poster=res.get('bimg'),
                    name=res.get('name'),
                )
                if not film.get('poster '):
                    for r in json.loads(r):
                        if r.get('bimg'):
                            film = dict(
                                id=r.get('id'),
                                poster=r.get('bimg'),
                                name=r.get('name'),
                            )
            elif service == 'youtube':
                res = re.search('<entry>(.+?)</entry>', r).group()
                posterTmp = re.search("<media:thumbnail(.+?)height='360'(.+?)/>", res).group()
                film = dict(
                    id=re.search('<id>(.+?)</id>', res).group().replace('<id>','').replace('</id>',''),
                    poster=re.search("url='(.+?)'",posterTmp).group().replace('url=','').replace("'",''),
                    name=re.search('<title (.+?)</title>', res).group().replace('</title>','').split('>')[1],
                )
            else:
                return
            self.__films[service] = film
            self.__logger.info('Received film info from service %s' % service)
            saveStatus(
                updatestatus='DONE',
                updatetime=datetime.now().strftime(dateFormat)
            )

        def collectUrlFail(f, service):
            self.__films[service] = dict(id='', poster='', name='')
            message = 'Failed by receiving info from service: %s.' % service
            self.__logger.critical(message)
            self.__logger.critical('Reason: %s.' % f)
            saveStatus(
                updatestatus='FAILED',
                updatetime=datetime.now().strftime(dateFormat)
            )

        for service in self.__urls.keys():
            p = getPage(str(self.__urls.get(service)))
            p.addCallback(collectUrlDone, service)
            p.addErrback(collectUrlFail, service)

    def collect_run(self):
        self.__logger.info('Updating info...')
        self.collect()
        self.__logger.info('Updating info done.')
        if self.app.getRunning():
            reactor.callLater(self.__refreshtime, self.collect_run)


class Application(object):
    __running = True

    def __init__(self, collector):
        self.__collector = collector
        self.__collector.app = self

    def getRunning(self):
        return self.__running

    def run(self):
        signal.signal(signal.SIGINT, self.stop)
        signal.signal(signal.SIGHUP, self.stop)
        saveStatus(service='START')
        threads.deferToThread(reactor.callLater, 0, self.__collector.collect_run)
        reactor.run()

    def stop(self, s, r):
        self.__running = False
        saveStatus(service='STOP')
        reactor.stop()


def main():
    #logger initialize
    log.setLevel(logging.INFO)
    log_format = '%(asctime)s %(name)s %(levelname)-8s %(funcName)+24s | %(message)s'
    formatter = logging.Formatter(log_format, datefmt=dateFormat)

    logdest = _config_.get('logdest', 'stdout')
    if logdest == 'stdout':
        hdl = logging.StreamHandler()
    elif logdest == 'syslog':
        hdl = logging.handlers.SysLogHandler(address='/dev/log')
    else:
        hdl = logging.FileHandler(filename=logdest, mode='a')

    hdl.setFormatter(formatter)
    log.addHandler(hdl)

    c = Collector(log)

    site = server.Site(UserApi(logger=log, collector=c))
    reactor.listenTCP(
        int(_config_.get('userapi', dict()).get('port', '8082')),
        site
    )

    app = Application(c)
    app.run()

if __name__ == '__main__':
    main()
